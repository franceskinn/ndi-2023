const Titre = ["Causes et Compréhension du Réchauffement","Impacts et Urgence du Réchauffement Climatique","Perceptions et Désinformation sur le Changement Climatique"];
let cont  = 0;
function showSlide(carouselId, index) {
    const carousel = document.getElementById(carouselId);
    const items = carousel.querySelectorAll('.carousel-item');
    const itemWidth = carousel.querySelector('.carousel-item').offsetWidth;
    const totalItems = items.length;
    var data = document.getElementById('Nikhil');
    data.textContent = Titre[0];
    // Ajustement de l'index pour qu'il reste dans les limites valides
    if (index >= totalItems) {
        index = 0;
    } else if (index < 0) {
        index = totalItems - 1; // Boucle à la dernière image si l'index est négatif
    }

    carousel.style.transform = `translateX(-${index * itemWidth}px)`;
    carousel.dataset.currentIndex = index; // Mise à jour de l'index actuel
}

function nextSlide(carouselId) {
    const carousel = document.getElementById(carouselId);
    let currentIndex = parseInt(carousel.dataset.currentIndex || 0);
    showSlide(carouselId, currentIndex + 1);
    cont = (cont+1)%3;
    var data = document.getElementById('Nikhil');
    data.textContent = Titre[cont];
}
function prevSlide(carouselId) {
    const carousel = document.getElementById(carouselId);
    let currentIndex = parseInt(carousel.dataset.currentIndex || 0);
    showSlide(carouselId, currentIndex - 1); // Décrémente l'index pour aller à la diapositive précédente
    cont = (cont+1)%3;
    var data = document.getElementById('Nikhil');
    data.textContent = Titre[2-cont];
}