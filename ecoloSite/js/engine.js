// Quiz result options in a separate object for flexibility
var resultOptions = [
    {   title: 'Si vous étiez président, l\'écologie tremblerait...',
        desc: '<p></p><img src="https://gitlabinfo.iutmontp.univ-montp2.fr/franceskinn/ndi-2023/-/raw/feature/quiz/ecoloSite/images/thisIsFine.jpg"/>'},
    {   title: 'HE OH ! Tu peux mieux faire là...',
        desc: '<p></p><img src="https://gitlabinfo.iutmontp.univ-montp2.fr/franceskinn/ndi-2023/-/raw/feature/quiz/ecoloSite/images/trumpIsSus.png"/>'},
    {   title: 'Mouais, c\'est pas trop ça mais ça passe.',
        desc: '<p></p><img src="https://gitlabinfo.iutmontp.univ-montp2.fr/franceskinn/ndi-2023/-/raw/feature/quiz/ecoloSite/images/confusedStonks.jpg"/>'},
    {   title: 'Pas mal du tout ! A deux allumettes d\'être un boss en écologie...',
        desc: '<p></p><img src="https://gitlabinfo.iutmontp.univ-montp2.fr/franceskinn/ndi-2023/-/raw/feature/quiz/ecoloSite/images/pepeTheChad.jpg"/>'},
    {   title: 'Bravo, Vous êtes inébranlable !',
        desc: '<p></p><img src="https://gitlabinfo.iutmontp.univ-montp2.fr/franceskinn/ndi-2023/-/raw/feature/quiz/ecoloSite/images/pouceMeme.png"/>'}
];
    
// global variables
var quizSteps = $('#quizzie .quiz-step'),
    totalScore = 0;

// for each step in the quiz, add the selected answer value to the total score
// if an answer has already been selected, subtract the previous value and update total score with the new selected answer value
// toggle a visual active state to show which option has been selected
quizSteps.each(function () {
    var currentStep = $(this),
        ansOpts = currentStep.children('.quiz-answer');
    // for each option per step, add a click listener
    // apply active class and calculate the total score
    ansOpts.each(function () {
        var eachOpt = $(this);
        eachOpt[0].addEventListener('click', check, false);
        function check() {
            var $this = $(this),
                value = $this.attr('data-quizIndex'),
                answerScore = parseInt(value);
            // check to see if an answer was previously selected
            if (currentStep.children('.active').length > 0) {
                var wasActive = currentStep.children('.active'),
                    oldScoreValue = wasActive.attr('data-quizIndex'),
                    oldScore = parseInt(oldScoreValue);
                // handle visual active state
                currentStep.children('.active').removeClass('active');
                $this.addClass('active');
                // handle the score calculation
                totalScore -= oldScoreValue;
                totalScore += answerScore;
                calcResults(totalScore);
            } else {
                // handle visual active state
                $this.addClass('active');
                // handle score calculation
                totalScore += answerScore;
                calcResults(totalScore);
                // handle current step
                updateStep(currentStep);
            }
        }
    });
});

// show current step/hide other steps
function updateStep(currentStep) {
    if(currentStep.hasClass('current')){
       currentStep.removeClass('current');
       currentStep.next().addClass('current');
    }
}

// display scoring results
async function calcResults(totalScore) {
    // only update the results div if all questions have been answered
    if (quizSteps.find('.active').length == quizSteps.length){
        var resultsTitle = $('#results h1'),
            resultsDesc = $('#results .desc');
        
        // calc lowest possible score
        var lowestScoreArray = $('#quizzie .low-value').map(function() {
            return $(this).attr('data-quizIndex');
        });
        var minScore = 0;
        for (var i = 0; i < lowestScoreArray.length; i++) {
            minScore += lowestScoreArray[i] << 0;
        }
        // calculate highest possible score
        var highestScoreArray = $('#quizzie .high-value').map(function() {
            return $(this).attr('data-quizIndex');
        });
        var maxScore = 0;
        for (var i = 0; i < highestScoreArray.length; i++) {
            maxScore += highestScoreArray[i] << 0;
        }
        // calc range, number of possible results, and intervals between results
        var range = maxScore - minScore,
            numResults = resultOptions.length,
            interval = range / (numResults - 1),
            increment = '',
            n = 0; //increment index
        // incrementally increase the possible score, starting at the minScore, until totalScore falls into range. then match that increment index (number of times it took to get totalScore into range) and return the corresponding index results from resultOptions object
        while (n < numResults) {
            increment = minScore + (interval * n);
            if (totalScore <= increment) {
                // populate results
                ratioScore = 439 - (totalScore - minScore)*(439 / range);
                document.documentElement.style.setProperty("--var-dash-offset", ratioScore);
                document.getElementById('score').textContent = totalScore + "/" + maxScore;
                if (ratioScore > 250) {
                    document.documentElement.style.setProperty("--var-circle-stroke", "rgb(223, 45, 45)");
                    document.documentElement.style.setProperty("--var-circle-shadow", "#fb5757");
                }
                else if (ratioScore > 100) {
                    document.documentElement.style.setProperty("--var-circle-stroke", "rgb(242, 166, 20)");
                    document.documentElement.style.setProperty("--var-circle-shadow", "#f7b900");
                }
                else {
                    document.documentElement.style.setProperty("--var-circle-stroke", "rgb(9, 158, 6)");
                    document.documentElement.style.setProperty("--var-circle-shadow", "#33ff36");
                    document.getElementById('confettiTarget').innerHTML = '<canvas id="drawing_canvas"></canvas>';
                    drawingCanvas = document.getElementById("drawing_canvas");
                    initDrawingCanvas(drawingCanvas);
                    requestAnimationFrame(loop);
                }
                resultsTitle.replaceWith("<h1>" + resultOptions[n].title + "</h1>");
                resultsDesc.replaceWith("<p class='desc'>" + resultOptions[n].desc + "</p>");
                document.getElementById('debunking').style.display = "block";
                await new Promise(r => setTimeout(r, 5000));
                newContent = document.getElementById('newContentCard');
                newContent.style.display = "flex";
                window.scrollBy(0, window.outerHeight, "smooth");
                await new Promise(r => setTimeout(r, 500));
                newContent.classList.add('slide-in-elliptic-top-fwd');
                await new Promise(r => setTimeout(r, 500));
                document.getElementById('contentCard').classList.add('rotate-in-2-fwd-ccw');
                unlockLevel(document.getElementById('level-num').value);
                return;
            } else {
                n++;
            }
        }
    }
}