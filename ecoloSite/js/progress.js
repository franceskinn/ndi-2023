function loadProgress() {
    if (!document.cookie) {
        document.cookie = "100100100";
    }

    var child = 0;
    var parents = document.getElementsByClassName('carousel-item');
    for (var j = 0; j < parents.length; j++) {
        var children = parents[j].children;
        for (var i = 0; i < children.length; i++) {
            if (document.cookie[child] == 0) {
                children[i].classList.add('locked');
            }
            child++;
        }
    }

}

function unlockLevel(j) {
    var a = document.cookie.split('');
    a[j] = 1;
    document.cookie = a.join("");
}