const I = 0;
const J = 1;
const L = 2;
const O = 3;
const S = 4;
const T = 5;
const Z = 6;

const blockRotations = [
    [
        createBoolMatrix(4, [false, false, false, false, true, true, true, true, false, false, false, false, false, false, false, false]),
        createBoolMatrix(4, [false, false, true, false, false, false, true, false, false, false, true, false, false, false, true, false]),
        createBoolMatrix(4, [false, false, false, false, false, false, false, false, true, true, true, true, false, false, false, false]),
        createBoolMatrix(4, [false, true, false, false, false, true, false, false, false, true, false, false, false, true, false, false])
    ],
    [
        createBoolMatrix(3, [true, false, false, true, true, true, false, false, false]),
        createBoolMatrix(3, [false, true, true, false, true, false, false, true, false]),
        createBoolMatrix(3, [false, false, false, true, true, true, false, false, true]),
        createBoolMatrix(3, [false, true, false, false, true, false, true, true, false])
    ],
    [
        createBoolMatrix(3, [false, false, true, true, true, true, false, false, false]),
        createBoolMatrix(3, [false, true, false, false, true, false, false, true, true]),
        createBoolMatrix(3, [false, false, false, true, true, true, true, false, false]),
        createBoolMatrix(3, [true, true, false, false, true, false, false, true, false])
    ],
    [
        createBoolMatrix(2, [true, true, true, true])
    ],
    [
        createBoolMatrix(3, [false, true, true, true, true, false, false, false, false]),
        createBoolMatrix(3, [false, true, false, false, true, true, false, false, true]),
        createBoolMatrix(3, [false, false, false, false, true, true, true, true, false]),
        createBoolMatrix(3, [true, false, false, true, true, false, false, true, false])
    ],
    [
        createBoolMatrix(3, [false, true, false, true, true, true, false, false, false]),
        createBoolMatrix(3, [false, true, false, false, true, true, false, true, false]),
        createBoolMatrix(3, [false, false, false, true, true, true, false, true, false]),
        createBoolMatrix(3, [false, true, false, true, true, false, false, true, false])
    ],
    [
        createBoolMatrix(3, [true, true, false, false, true, true, false, false, false]),
        createBoolMatrix(3, [false, false, true, false, true, true, false, true, false]),
        createBoolMatrix(3, [false, false, false, true, true, false, false, true, true]),
        createBoolMatrix(3, [false, true, false, true, true, false, true, false, false])
    ]

];

class Tetromino
{

    #sides = [0, 0, 0];
    
    #rotation = 0;
    #type = 0;

    #rotations;
    
    constructor(type)
    {
        this.#type = type;
        
        this.#rotations = blockRotations[type];

        if(type == O)
        {
            this.#sides = [0, 1, 1];
            return;
        }

        this.applyRotation();
    }

    rotate(clockwise)
    {
        if(this.#type == O)
        {
            return 0;
        }

        if(clockwise)
        {
            this.#rotation = (this.#rotation + 1) % 4;
            this.applyRotation();
            return this.#rotation;
        }
        
        if (this.#rotation == 0)
        {
            this.#rotation = 3;
            this.applyRotation();
            return this.#rotation;
        }

        this.#rotation--;
        this.applyRotation();
        return this.#rotation;
    }

    applyRotation()
    {
        let shape = this.getRotatedShape();

        this.#sides = [this.getLeftMost(shape), this.getRightMost(shape), this.getLowest(shape)]; 
    }

    getLeftMost(matrix)
    {
        let leftMost = 0;
        let foundLine = false;

        while (leftMost < matrix.size())
        {
            if(!foundLine && !matrix.getColumn(leftMost))
            {
                leftMost++;
                continue;
            }

            foundLine = foundLine && matrix.getColumn(leftMost);
            if(!foundLine)
            {
                return leftMost;
            }
            leftMost++;
        }
        return leftMost;
    }

    getRightMost(matrix)
    {
        let rightMost = matrix.size() - 1;
        let foundLine = false;

        while (rightMost >= 0)
        {
            if (!foundLine && !matrix.getColumn(rightMost))
            {
                rightMost--;
                continue;
            }

            foundLine = foundLine && matrix.getColumn(rightMost);
            if (!foundLine)
            {
                return rightMost;
            }
            rightMost--;
        }
        return rightMost;
    }

    getLowest(matrix)
    {
        let lowest = matrix.size() - 1;
        while (lowest >= 0 && !matrix.getRow(lowest))
        {
            lowest--;
        }
        return lowest;
    }

    getRotationIndex()
    {
        return this.#rotation;
    }

    getRotatedShape()
    {
        return this.#rotations[this.#rotation];
    }
    
    getShapeSize()
    {
        return this.#rotations[0].size();
    }

    getSides()
    {
        return this.#sides;
    }

    getType()
    {
        return this.#type;
    }

}
