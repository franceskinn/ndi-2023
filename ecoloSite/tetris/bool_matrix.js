class BoolMatrix
{

    #sqrSize = 0;
    #size = 0;

    #bools;

    constructor(size)
    {
        this.#sqrSize = size * size;
        this.#size = size;

        this.#bools = new Array(this.#sqrSize).fill(false);
    }

    getColumn(col)
    {
        let row = 0;
        let res = false;

        while(!res && row < this.#size)
        {
            res = this.#bools[row * this.#size + col];
            row++;
        }
        return res;
    }
    
    getRow(row)
    {
        let col = 0;
        let res = false;

        while(!res && col < this.#size)
        {
            res = this.#bools[row * this.#size + col];
            col++;
        }
        return res;
    }

    get(x, y)
    {
        return this.#bools[x * this.#size + y];
    }

    size()
    {
        return this.#size;
    }

    squareSize()
    {
        return this.#sqrSize;
    }

    fill(elements)
    {
        this.#bools = elements;
    }

}

function createBoolMatrix(size, values)
{
    matrix = new BoolMatrix(size);
    matrix.fill(values);
    return matrix;
}