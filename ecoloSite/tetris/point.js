class Point
{
    #x;
    #y;

    constructor(x, y)
    {
        this.#x = x;
        this.#y = y;
    }

    x()
    {
        return this.#x;
    }

    y()
    {
        return this.#y;
    }

    invert()
    {
        return new Point(-this.#x, -this.#y);
    }
    
    add(point)
    {
        return new Point(this.#x + point.x(), this.#y + point.y());
    }

}