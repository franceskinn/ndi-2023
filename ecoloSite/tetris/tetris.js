const messageFin = document.getElementById("messageFin");
const scoreText = document.getElementById("score");

const tPieceCorners = [ new Point(0, 0), new Point(0, 2), new Point(2, 2), new Point(2, 0) ];

const IWallKicks = 
[
    [new Point(0, 0), new Point(-2, 0), new Point(1, 0), new Point(-2, 1), new Point(1, -2)],
    [new Point(0, 0), new Point(-1, 0), new Point(2, 0), new Point(-1, -2), new Point(2, 1)],
    [new Point(0, 0), new Point(2, 0), new Point(-1, 0), new Point(2, -1), new Point(-1, 2)],
    [new Point(0, 0), new Point(1, 0), new Point(-2, 0), new Point(1, 2), new Point(-2, -1)]  
];

const JLSTZWallKicks =
[
    [new Point(0, 0), new Point(-1, 0), new Point(-1, -1), new Point(0, 2), new Point(-1, 2)],
    [new Point(0, 0), new Point(1, 0), new Point(1, 1), new Point(0, -2), new Point(1, -2)],
    [new Point(0, 0), new Point(1, 0), new Point(1, -1), new Point(0, 2), new Point(1, 2)],
    [new Point(0, 0), new Point(-1, 0), new Point(-1, 1), new Point(0, -2), new Point(-1, -2)]
];

const framerate = 1000 / 60;

const gridHeight = 20;
const gridWidth = 10;

const softDropFactor = 6.0;
const gravity = 1.0;

const grid = new Array(gridWidth);

var bag = new Array(5);

var hold = -1;
var holded = false;

var movedHorizontally = false;
var frameDelay = 0;

var tetrominoPosX = 0;
var tetrominoPosY = 0;

var piecesDone = 0;

var fallTimer = 0.0;
var currentGravity = 0.0;

var tSpin = false;

var score = 0;

function getBit(number, bitToGet)
{
    return (number & (1 << bitToGet)) >> bitToGet;
}

function generateBag()
{
    let bagSize = 0;
    while (bagSize < 5)
    {
        piece = Math.round(Math.random() * 6);
        if (getBit(piecesDone, piece) === 0)
        {
            bag[bagSize] = piece;
            piecesDone |= 1 << piece;
            bagSize++;
        }

    }

}

function init()
{
    for(let i = 0; i < gridWidth; i++)
    {
        grid[i] = new Array(gridHeight).fill(-1);
    }

    let pieceID = Math.round(Math.random() * 6);
    currentPiece = new Tetromino(pieceID);
    piecesDone |= 1 << pieceID;

    tetrominoPosX = Math.floor(5 - currentPiece.getShapeSize() / 2);
    tetrominoPosY = -currentPiece.getSides()[2];

    generateBag();

    currentGravity = gravity;
    fallTimer = currentGravity;
}

function checkSide(xMove)
{
    if(xMove === 0)
    {
        return 0;
    }

    let sides = currentPiece.getSides();
    let newPos = tetrominoPosX + xMove;

    if(newPos >= 10 - sides[1])
    {
        return 0;
    }
    
    if (newPos < -sides[0])
    {
        return 0;
    }

    let blockOnSide = false;
    let blockSize = currentPiece.getShapeSize();

    if(xMove < 0)
    {
        let lineLMost = sides[0];
        while(!blockOnSide && lineLMost < blockSize)
        {
            let y = 0;
            while(!blockOnSide && y <= sides[2])
            {
                let posY = tetrominoPosY + y;
                if(posY >= 0 && currentPiece.getRotatedShape().get(y, lineLMost))
                {
                    blockOnSide = grid[newPos + lineLMost][posY] !== -1;
                }
                y++;
            }
            lineLMost++;
        }
        return blockOnSide ? 0 : xMove;
    }

    let lineRMost = sides[1];
    while (!blockOnSide && lineRMost >= 0)
    {
        let y = 0;
        while (!blockOnSide && y <= sides[2])
        {
            let posY = tetrominoPosY + y;
            if (posY >= 0 && currentPiece.getRotatedShape().get(y, lineRMost))
            {
                blockOnSide = grid[newPos + lineRMost][posY] !== -1;
            }
            y++;
        }
        lineRMost--;
    }
    return blockOnSide ? 0 : xMove;
}

function placePiece()
{
    for (let line = 0; line <= currentPiece.getSides()[2]; line++)
    {
        for (let block = 0; block < currentPiece.getShapeSize(); block++)
        {
            if (currentPiece.getRotatedShape().get(line, block))
            {
                grid[tetrominoPosX + block][tetrominoPosY + line] = currentPiece.getType();
            }

        }

    }

    let lineCleared = 0;
    let lowestLineCleared = 0;

    for (let yLine = 0; yLine < 20; yLine++)
    {
        let sameValue = true;
        let xLine = 0;

        while (sameValue && xLine < 10)
        {
            sameValue = grid[xLine][yLine] !== -1;
            xLine++;
        }

        if (sameValue)
        {
            lineCleared += 1;
            lowestLineCleared = yLine;
        }

    }

    for (let yLine = 0; yLine < lineCleared; yLine++)
    {
        for (let xLine = 0; xLine < 10; xLine++)
        {
            grid[xLine][lowestLineCleared - yLine] = -1;
        }

    }

    if (lineCleared != 0)
    {
        let fullClear = true;
        let xCheck = 0;

        while (fullClear && xCheck < 10)
        {
            let yCheck = 0;
            while (fullClear && yCheck < 20)
            {
                fullClear = grid[xCheck][yCheck] === -1;
                yCheck++;
            }
            xCheck++;
        }

        if (!fullClear)
        {
            for (let yLine = lowestLineCleared; yLine >= 0; yLine--)
            {
                for (let xLine = 0; xLine < 10; xLine++)
                {
                    if(yLine < lineCleared)
                    {
                        grid[xLine][yLine] = -1;
                        continue;
                    }
                    grid[xLine][yLine] = grid[xLine][yLine - lineCleared];
                }

            }

        }

        switch (lineCleared)
        {
            case 4:
                if (fullClear)
                {
                    score += 20;
                    messageFin.textContent = "Nettoyage complet par 4 lignes ! (+20 points)";
                    break;
                }
                messageFin.textContent = "Tetris ! (+4 points)"
                score += 4;
                break;
            case 3:
                if (fullClear)
                {
                    score += 20;
                    messageFin.textContent = "Nettoyage complet par 3 lignes ! (+20 points)";
                    break;
                }

                if (tSpin)
                {
                    score += 8;
                    messageFin.textContent = "T-Spin triple ! (+8 points)";
                    break;
                }

                score += 3;
                break;
            case 2:
                if (fullClear)
                {
                    score += 20;
                    messageFin.textContent = "Nettoyage complet par 2 lignes ! (+20 points)";
                    break;
                }

                if (tSpin)
                {
                    score += 6;
                    messageFin.textContent = "T-Spin ! (+6 points)";
                    break;
                }

                score += 2;
                break;
            default:
                if (fullClear)
                {
                    score += 20;
                    messageFin.textContent = "Nettoyage complet par 1 ligne ! (+20 points)";
                    break;
                }

                score += 1;
                break;
        }

    }

    scoreText.textContent = "Score : " + score;
    if(score >= 404)
    {
        currentPiece = undefined;
        messageFin.textContent = "Bravo, vous avez perdu votre temps avec brillio !";
        return true;
    }

    nextPiece();

    tSpin = false;
    tetrominoPosX = Math.floor(5 - currentPiece.getShapeSize() / 2);
    tetrominoPosY = -currentPiece.getSides()[2];

    if (overlaps(new Point(tetrominoPosX, tetrominoPosY + 1)))
    {
        currentPiece = undefined;
        messageFin.textContent = "Fin de la partie ! Vous n'avez pas réussi...";
        return true;
    }

    return false;
}

function nextPiece()
{
    currentPiece = new Tetromino(bag[0]);
    for (let i = 0; i < 4; i++)
    {
        bag[i] = bag[i + 1];
    }

    let lastPiece = 0;
    while (getBit(piecesDone, lastPiece) !== 0)
    {
        lastPiece = Math.round(Math.random() * 6);
    }

    bag[4] = lastPiece;
    piecesDone |= 1 << lastPiece;

    if (piecesDone == 127)
    {
        piecesDone = 0;
    }

}

function update(inputs, oneFrameInputs)
{
    drawNext(bag);
    if(currentPiece === undefined)
    {
        return false;
    }

    drawTetromino(tetrominoPosX, tetrominoPosY, currentPiece.getRotatedShape(), colors[currentPiece.getType()]);
    if(hold !== -1)
    {
        drawTetromino(-4, 1, blockRotations[hold][0], colors[hold]);
    }

    if (oneFrameInputs.includes("Space"))
    {
        let toHold = currentPiece.getType();
        if (hold !== -1)
        {
            let next = hold;
            currentPiece = new Tetromino(next);
        }
        else
        {
            nextPiece();
        }

        tetrominoPosX = Math.floor(5 - currentPiece.getShapeSize() / 2);
        tetrominoPosY = -currentPiece.getSides()[2];

        hold = toHold;
        return false;
    }

    let oldHMove = movedHorizontally;

    let xMove = 0;
    if (inputs.includes("KeyA"))
    {
        if(oldHMove && delay !== 0)
        {
            delay--;
        }
        else
        {
            xMove -= 1;
            delay = 3;
        }
        movedHorizontally = true;        
    }
    
    if (inputs.includes("KeyD"))
    {
        if(oldHMove && delay !== 0)
        {
            delay--;
        }
        else
        {
            xMove += 1;
            delay = 3;
        }
        movedHorizontally = true;
    }

    xMove = checkSide(xMove);

    let rotation = 0;
    if (oneFrameInputs.includes("KeyK"))
    {
        rotation = -1;
    }

    if (oneFrameInputs.includes("Semicolon"))
    {
        rotation = 1;
    }

    checkSRS(rotation);

    let yMove = 0;

    currentGravity = 1 / gravity;
    if (inputs.includes("KeyS"))
    {
        currentGravity /= softDropFactor;
    }

    if (fallTimer >= currentGravity)
    {
        fallTimer -= currentGravity;
        yMove = 1;
    }
    fallTimer += framerate / 1000;

    let blocksBelow = true;
    if (tetrominoPosY + currentPiece.getSides()[2] + yMove < 20)
    {
        blocksBelow = false;
        let toCheck = currentPiece.getSides()[2];
        while (!blocksBelow && toCheck >= 0)
        {
            let block = 0;
            while (!blocksBelow && block < currentPiece.getShapeSize())
            {
                if (tetrominoPosY + toCheck + yMove >= 0 && currentPiece.getRotatedShape().get(toCheck, block))
                {
                    blocksBelow = grid[tetrominoPosX + block][tetrominoPosY + toCheck + yMove] !== -1;
                }
                block++;
            }
            toCheck--;
        }

    }

    if(blocksBelow)
    {
        yMove = 0;
        
        if(placePiece())
        {
            return true;
        }
        return false;
    }

    tetrominoPosX += xMove;
    tetrominoPosY += yMove;
    return false;
}

function checkTSpin()
{
    let cornerCount = 0;
    for(let i = 0; i < tPieceCorners.length; i++)
    {
        xCheck = tetrominoPosX + tPieceCorners[i].x();
        yCheck = tetrominoPosY + tPieceCorners[i].y();

        if (yCheck >= 0 && grid[xCheck][yCheck] !== -1)
        {
            cornerCount++;
        }

    }
    
    tSpin = cornerCount >= 3; 
}

function overlaps(checkPosition)
{
    let overlaps = false;
    let sides = currentPiece.getSides();

    let x = sides[0];

    while(!overlaps && x <= sides[1])
    {
        let y = 0;
        while(!overlaps && y <= sides[2])
        {
            if (checkPosition.y() + y >= 20 || checkPosition.x() + x < 0 || checkPosition.x() + x >= 10)
            {
                overlaps = true;
                continue;
            }
            overlaps = checkPosition.y() + y >= 0 && currentPiece.getRotatedShape().get(x, y) && grid[checkPosition.x() + x][checkPosition.y() + y] !== -1;
            y++;
        }
        x++;
    }
    return overlaps;
}

function getTable(rotation)
{
    let oldRotation = currentPiece.getRotationIndex();
    let newRotation = currentPiece.rotate(rotation == 1);

    if ((oldRotation === 1 && newRotation === 2) || (oldRotation === 2 && newRotation === 1))
    {
        return 1;
    }

    if ((oldRotation === 2 && newRotation === 3) || (oldRotation === 3 && newRotation === 2))
    {
        return 2;
    }

    if ((oldRotation === 3 && newRotation === 0) || (oldRotation === 0 && newRotation === 3))
    {
        return 3;
    }
    return 0;
}

function checkSRS(rotInput)
{
    if (currentPiece === undefined || currentPiece.getType() === O)
    {
        return;
    }

    if (rotInput === 0)
    {
        return;
    }

    if (currentPiece.getType() === I)
    {
        checkRotationPlacement(IWallKicks, rotInput);
        return;
    }
    checkRotationPlacement(JLSTZWallKicks, rotInput);
}

function checkRotationPlacement(wallKickTable, rotation)
{
    tSpin = false;

    let check = false;
    let test = 0;
    let table = getTable(rotation);

    while(!check && test < 5)
    {
        let kick = wallKickTable[table][test];
        if(rotation < 0)
        {
            kick = kick.invert();
        }
        
        checkPosition = new Point(tetrominoPosX + kick.x(), tetrominoPosY + kick.y());
        
        if(!overlaps(checkPosition))
        {
            check = true;
            break;
        }
        test++;
    }

    if(!check)
    {
        currentPiece.rotate(rotation == -1);
        return;
    }

    tetrominoPosX += rotation * wallKickTable[table][test].x();
    tetrominoPosY += rotation * wallKickTable[table][test].y();

    if (currentPiece.getType() == T)
    {
        checkTSpin();
    }

}